<?php 
$anchor = (get_sub_field('anchor')) ? ' id="'.get_sub_field('anchor').'"' : '';
$markers_group = get_sub_field('markers_group');
?>
<section class="infrastructure__section scroll"<?php echo $anchor; ?>>
    <span class="dots"></span>
    <div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title text-left" data-aos="fade-up" data-aos-duration="1500">
                    <h2><?php the_sub_field('title'); ?></h2>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    <?php if( $markers_group ) { ?>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="map__sorting" data-aos="fade-up" data-aos-duration="1500">
                    <ul>
                    <?php 
                    foreach ( $markers_group as $marker_group ) { ?>
                        <li>
                            <?php if( $marker_group['group_icon'] ) { ?>
                                <img src="<?php echo $marker_group['group_icon']; ?>" alt="<?php echo $marker_group['group_title']; ?>">
                            <?php }
                            if( $marker_group['group_icon_hover'] ) { ?>
                                <img src="<?php echo $marker_group['group_icon_hover']; ?>" alt="<?php echo $marker_group['group_title']; ?>" class="hover">
                            <?php }
                            if( $marker_group['group_title'] ) { ?>
                                <h4><?php echo $marker_group['group_title']; ?></h4>
                            <?php }
                            if( $marker_group['markers'] ) { ?>
                                <div class="markers" data-marker="<?php echo $marker_group['markers_icon']; ?>">
                                    <?php foreach ($marker_group['markers'] as $marker) { ?>
                                        <div class="marker" data-lat="<?php echo $marker['latitude']; ?>" data-lng="<?php echo $marker['longitude']; ?>"></div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </li>
                    <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="map__block" data-aos="fade-up" data-aos-duration="1500">
                    <div class="map__wrapper" id="map-canvas"
                         data-lat="<?php the_sub_field('latitude'); ?>"
                         data-lng="<?php the_sub_field('longitude'); ?>"
                         data-zoom="<?php the_sub_field('zoom'); ?>"
                         data-marker="<?php the_sub_field('marker'); ?>">
                    </div>
                    <div class="location__list">
                        <a class="marker" data-rel="map-canvas" data-lat="<?php the_sub_field('latitude'); ?>"
                         data-lng="<?php the_sub_field('longitude'); ?>"></a>
                    </div>
                </div>
            </div>
        </div>
</section>