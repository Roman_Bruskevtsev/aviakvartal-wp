<?php 
$anchor = (get_sub_field('anchor')) ? ' id="'.get_sub_field('anchor').'"' : '';
$images = get_sub_field('images');
?>
<section class="documentation__section scroll"<?php echo $anchor; ?>>
    <div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title text-center" data-aos="fade-up" data-aos-duration="1500">
                    <h2><?php the_sub_field('title'); ?></h2>
                </div>
            </div>
        </div>
        <?php } 
        if($images) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="documents__slider">
                    <?php foreach ($images as $image) { 
                        $background = ( $image ) ? ' style="background-image:url('.$image['url'].')"' : '';
                        ?>
                        <div class="slide">
                            <a class="image"<?php echo $background; ?> href="<?php echo $image['url']; ?>" target="_blank"></a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</section>