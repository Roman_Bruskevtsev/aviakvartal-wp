<?php 
$anchor = (get_sub_field('anchor')) ? ' id="'.get_sub_field('anchor').'"' : '';
?>
<section class="contacts__section scroll"<?php echo $anchor; ?>>
    <?php if( get_sub_field('image') ) { 
        $image = ' style="background-image: url('.get_sub_field('image')['url'].')"';
    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7">
                <div class="image__block"<?php echo $image; ?>></div>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-5 offset-lg-7">
                <div class="contact__block">
                    <?php if( get_sub_field('title') ) { ?>
                    <div class="section__title text-left" data-aos="fade-left" data-aos-duration="1500">
                        <h2><?php the_sub_field('title'); ?></h2>
                    </div>
                    <?php } ?>
                    <?php if( get_sub_field('contacts') ) { ?>
                    <div class="contacts__information" data-aos="fade-left" data-aos-duration="1500">
                        <?php the_sub_field('contacts'); ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        
    </div>
</section>