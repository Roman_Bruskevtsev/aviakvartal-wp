<?php 
$planning = get_sub_field('planning_apartment');
$payment = get_sub_field('terms_of_payment');
?>
<section class="planning__section">
    <span class="spot__1"></span>
    <span class="spot__2"></span>
    <div class="container">
    <?php if( $planning ) { 
        $anchor = ($planning['anchor']) ? ' id="'.$planning['anchor'].'"' : '';
        if( $planning['title'] ) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title text-left scroll"<?php echo $anchor; ?> data-aos="fade-up" data-aos-duration="1500">
                    <h2><?php echo $planning['title']; ?></h2>
                </div>
            </div>
        </div>
        <?php } 
        if( $planning['tabs'] ) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="planning__tabs tabs" data-aos="fade-up" data-aos-duration="1500">
                    <div class="tabs__nav">
                        <div class="row">
                            <div class="col-lg-0 col-xl-7"></div>
                            <div class="col-lg-12 col-xl-5">
                            <?php $i = 0;
                            foreach ($planning['tabs'] as $tab) { ?>
                                <div class="tab tab__<?php echo $i; if($i == 0) echo ' active'; ?>">
                                    <span><?php echo $tab['title']; ?></span>
                                </div>
                            <?php $i++; } ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab__blocks">
                    <?php $j = 0;
                    foreach ($planning['tabs'] as $tab) { ?>
                        <div class="tab__block tab__<?php echo $j; if($j == 0) echo ' active'; ?>">
                            <div class="row">
                                <div class="col-lg-6">
                                    <?php if( $tab['image_1'] ) { ?>
                                    <div class="render__block">
                                        <img src="<?php echo $tab['image_1']['url']; ?>" alt="<?php echo $tab['image_1']['title']; ?>">
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="col-lg-6">
                                    <div class="plan__block">
                                        <?php if( $tab['image_2'] ) { ?>
                                        <div class="image">
                                            <img src="<?php echo $tab['image_2']['url']; ?>" alt="<?php echo $tab['image_2']['title']; ?>">
                                        </div>
                                        <?php } ?>
                                        <?php
                                        $details = $tab['details'];
                                        if( $details ) { ?>
                                        <div class="details">
                                            <?php foreach ($details as $detail) { ?>
                                            <div class="column">
                                                <?php if( $detail['title'] ) { ?>
                                                    <div class="title">
                                                        <?php echo $detail['title']; ?>
                                                    </div>
                                                <?php } 
                                                if( $detail['value'] ) { ?>
                                                    <div class="value">
                                                        <?php echo $detail['value']; ?>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $j++; } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php } 
    } ?>
    <?php if( $payment ) { 
        $anchor = ($payment['anchor']) ? ' id="'.$payment['anchor'].'"' : '';
        if( $payment['title'] ) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title text-center scroll"<?php echo $anchor; ?> data-aos="fade-up" data-aos-duration="1500">
                    <h2><?php echo $payment['title']; ?></h2>
                </div>
            </div>
        </div>
        <?php } 
        if( $payment['tabs'] ) { ?>
        <div class="row justify-content-md-center">
            <div class="col-lg-7">
                <div class="payment__tabs tabs" data-aos="fade-up" data-aos-duration="1500">
                    <div class="tabs__nav text-center">
                    <?php $i = 0;
                    foreach ($payment['tabs'] as $tab) { ?>
                        <div class="tab tab__<?php echo $i; if($i == 0) echo ' active'; ?>">
                            <span><?php echo $tab['title']; ?></span>
                        </div>
                    <?php $i++; } ?>
                    </div>
                    <div class="tab__blocks">
                    <?php $j = 0;
                    foreach ($payment['tabs'] as $tab) { ?>
                        <div class="tab__block tab__<?php echo $j; if($j == 0) echo ' active'; ?>">
                        <?php foreach ( $tab['rows'] as $row ) { ?>
                            <div class="tab__row">
                                <?php if( $row['column_1'] ) { ?>
                                    <div class="column">
                                        <span class="text"><?php echo $row['column_1'] ; ?></span>
                                        <?php if( $row['column_1_description'] ) { ?><span class="desc"><?php echo $row['column_1_description'] ; ?></span><?php } ?>
                                    </div>
                                <?php } ?>
                                <?php if( $row['column_2'] ) { ?>
                                    <div class="column">
                                        <span class="text"><?php echo $row['column_2'] ; ?></span>
                                        <?php if( $row['column_2_description'] ) { ?><span class="desc"><?php echo $row['column_2_description'] ; ?></span><?php } ?>
                                    </div>
                                <?php } ?>
                                <?php if( $row['column_3'] ) { ?>
                                    <div class="column summary">
                                        <span class="text"><?php echo $row['column_3'] ; ?></span>
                                        <?php if( $row['column_3_description'] ) { ?><span class="desc"><?php echo $row['column_3_description'] ; ?></span><?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        </div>
                    <?php $j++; } ?>
                    </div>
                    <?php if( $payment['button_label'] ) { ?>
                    <div class="tabs__footer text-center">
                        <button class="btn gradient__btn popup__btn">
                            <span class="text"><?php echo $payment['button_label']; ?></span>
                        </button>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>
    <?php } ?>
    </div>
</section>