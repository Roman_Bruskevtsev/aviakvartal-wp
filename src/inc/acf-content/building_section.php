<?php 
$anchor = (get_sub_field('anchor')) ? ' id="'.get_sub_field('anchor').'"' : '';
?>
<section class="building__section"<?php echo $anchor; ?>>
    <div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title text-center no__border" data-aos="fade-up" data-aos-duration="1500">
                    <h2><?php the_sub_field('title'); ?></h2>
                    <?php if( get_sub_field('date') ) { ?><p><?php the_sub_field('date'); ?></p><?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php 
        $images = get_sub_field('images');
        if( get_sub_field('images') ) { 
            $big_image = ' style="background-image: url('.$images[0]['url'].')"';
        ?>
        <div class="row">
            <div class="col-lg-6">
                <div class="image__block"<?php echo $big_image; ?> data-aos="fade-right" data-aos-duration="1500"></div>
            </div>
            <div class="col-lg-6">
                <div class="small__image__block" data-aos="fade-left" data-aos-duration="1500">
                    <div class="row">
                        <?php for ($i=1; $i < 5; $i++) { 
                            $small_image = ' style="background-image: url('.$images[$i]['url'].')"'; ?>
                            <div class="col-md-6 col-lg-6">
                                <div class="small__image"<?php echo $small_image; ?>></div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    <span class="dots"></span>
</section>