<?php 
$anchor = (get_sub_field('anchor')) ? ' id="'.get_sub_field('anchor').'"' : '';
?>
<section class="about__complex scroll"<?php echo $anchor; ?>>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <?php if( get_sub_field('image_block') ) { 
                    $image = (get_sub_field('image_block')) ? ' style="background-image:url('.get_sub_field('image_block')['url'].')"' : '';
                ?>
                    <div class="image__block"<?php echo $image; ?> data-aos="fade-up" data-aos-duration="1500"></div>
                <?php } ?>
            </div>
            <div class="col-lg-6">
                <div class="text__block" data-aos="fade-left" data-aos-duration="1500">
                <?php if( get_sub_field('title') ) { ?>
                    <div class="title">
                        <h5><?php the_sub_field('title'); ?></h5>
                    </div>
                <?php }
                if( get_sub_field('subtitle') ) { ?>
                    <h2><?php the_sub_field('subtitle'); ?></h2>
                <?php } ?>
                    <div class="info__block">
                        <?php if( get_sub_field('block_title_1') ) { ?>
                            <h4><?php the_sub_field('block_title_1');  ?></h4>
                        <?php } ?>
                        <?php if( get_sub_field('block_text_1') ) { ?>
                            <p><?php the_sub_field('block_text_1');  ?></p>
                        <?php } ?>
                    </div>
                    <div class="info__block">
                        <?php if( get_sub_field('block_title_2') ) { ?>
                            <h4><?php the_sub_field('block_title_2');  ?></h4>
                        <?php } ?>
                        <?php if( get_sub_field('block_text_2') ) { ?>
                            <p><?php the_sub_field('block_text_2');  ?></p>
                        <?php } ?>
                    </div>
                    <div class="info__block">
                        <?php if( get_sub_field('block_title_3') ) { ?>
                            <h4><?php the_sub_field('block_title_3');  ?></h4>
                        <?php } ?>
                        <?php if( get_sub_field('block_text_3') ) { ?>
                            <p><?php the_sub_field('block_text_3');  ?></p>
                        <?php } ?>
                    </div>
                    <?php if( get_sub_field('description') ) { ?>
                        <div class="description">
                            <?php the_sub_field('description');  ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <span class="dots"></span>
</section>