<?php 
$anchor = (get_sub_field('anchor')) ? ' id="'.get_sub_field('anchor').'"' : '';
?>
<section class="benefits"<?php echo $anchor; ?>>
    <div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title text-center" data-aos="fade-up" data-aos-duration="1500">
                    <h2><?php the_sub_field('title'); ?></h2>
                </div>
            </div>
        </div>
        <?php } 
        if( have_rows('benefits') ) { ?>
        <div class="row">
        <?php 
        $i = 1;
        while ( have_rows('benefits') ) : the_row(); ?>
            <div class="col-md-6 col-lg-6 col-xl-4">
                <div class="benefit__block" data-aos="fade-up" data-aos-duration="1500">
                    <?php if( get_sub_field('icon') ) { ?>
                        <img src="<?php echo get_sub_field('icon')['url'] ?>" alt="<?php echo get_sub_field('icon')['title']; ?>" class="icon">
                    <?php } ?>
                    <span class="number">0<?php echo $i; ?></span>
                    <div class="content">
                        <?php if( get_sub_field('title') ) { ?>
                            <h4><?php the_sub_field('title'); ?></h4>
                        <?php } ?>
                        <?php if( get_sub_field('text') ) { ?>
                            <p><?php the_sub_field('text'); ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php 
        $i++;
        endwhile; ?>
        </div>
        <?php } ?>
    </div>
</section>