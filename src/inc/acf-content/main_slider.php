<?php 
$anchor = (get_sub_field('anchor')) ? ' id="'.get_sub_field('anchor').'"' : '';
?>
<section class="slider__section scroll"<?php echo $anchor; ?>>
    <?php if( have_rows('slider') ) { ?>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-lg-12">
                <span class="dots"></span>
                <div class="main__slider">
                <?php while ( have_rows('slider') ) : the_row();
                    $image = (get_sub_field('image')) ? ' style="background-image:url('.get_sub_field('image')['url'].')"' : '';
                ?>
                    <div class="slide">
                        <div class="content">
                            <div class="text__block">
                                <?php if( get_sub_field('title') ) { ?>
                                <div class="title">
                                    <h5><?php the_sub_field('title'); ?></h5>
                                </div>
                                <?php } ?>
                                <?php if( get_sub_field('subtitle') ) { ?><h2 class="h1"><?php the_sub_field('subtitle'); ?></h2><?php } ?>
                                <?php the_sub_field('description'); ?>
                                <?php if( get_sub_field('link_url') ) { ?>
                                    <a href="<?php the_sub_field('link_url'); ?>" class="btn gradient__btn">
                                        <span class="text"><?php the_sub_field('link_label'); ?></span>
                                    </a>
                                <?php } ?>
                            </div>
                            <div class="image__block"<?php echo $image; ?>></div>
                        </div>
                    </div>
                <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 offset-lg-8">
                <div class="main__slider__nav">
                    <span class="prev__slide"></span>
                    <span class="next__slide"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 offset-lg-1">
                <div class="thumbnails__slider">
                <?php while ( have_rows('slider') ) : the_row(); 
                $image = (get_sub_field('image')) ? ' style="background-image:url('.get_sub_field('image')['url'].')"' : '';
                ?>
                    <div class="slide">
                        <div class="block"<?php echo $image; ?>>
                            <?php if( get_sub_field('title') ) { ?>
                                <h6><?php the_sub_field('title'); ?></h6>
                            <?php } ?>
                        </div>
                    </div>
                <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</section>
