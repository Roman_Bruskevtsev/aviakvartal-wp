<section class="form__section">
    <div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title text-center white__text no__border" data-aos="fade-up" data-aos-duration="1500">
                    <h2><?php the_sub_field('title'); ?></h2>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php if( get_sub_field('form_shortcode') ) { ?>
        <div class="row">
            <div class="col-lg-12">
                <?php echo do_shortcode( get_sub_field('form_shortcode') ); ?>
            </div>
        </div>
        <?php } ?>
    </div>
</section>