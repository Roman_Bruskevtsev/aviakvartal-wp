<?php
/**
 *
 * @package WordPress
 * @subpackage Aviakvartal
 * @since 1.0
 */

/*ACF Import*/
require get_template_directory() . '/inc/acf-import.php';


/*Theme setup*/
function aviakvartal_setup() {
    load_theme_textdomain( 'aviakvartal' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );

    add_image_size( 'slider-thumbnail', 220, 110, true );
    // add_image_size( 'portfolio-thumbnail', 650, 330, true );
    // add_image_size( 'portfolio-medium', 825, 464, true );

    register_nav_menus( array(
        'main'          => __( 'Main Menu', 'aviakvartal' )
    ) );
}
add_action( 'after_setup_theme', 'aviakvartal_setup' );

/*App2drive styles and scripts*/
function aviakvartal_scripts() {
    $version = '1.0.6';

    wp_enqueue_style( 'aviakvartal-css', get_theme_file_uri( '/assets/css/main.min.css' ), '', $version );
    wp_enqueue_style( 'aviakvartal-style', get_stylesheet_uri() );

    if( get_field( 'google_map_api', 'option' ) ) wp_enqueue_script( 'google-maps-key', 'https://maps.googleapis.com/maps/api/js?key='.get_field( 'google_map_api', 'option' ).'&libraries=places', array( 'jquery' ), $version, true );
    wp_enqueue_script( 'mousewheel-smoothscroll-js', get_theme_file_uri( '/assets/js/mousewheel-smoothscroll.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'aos-js', get_theme_file_uri( '/assets/js/aos.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'slick-js', get_theme_file_uri( '/assets/js/slick.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'script-js', get_theme_file_uri( '/assets/js/main.min.js' ), array( 'jquery' ), $version, true );

}
add_action( 'wp_enqueue_scripts', 'aviakvartal_scripts' );

/*Theme settings*/
if( function_exists('acf_add_options_page') ) {
    $general = acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'redirect'      => false,
        'capability'    => 'edit_posts',
        'menu_slug'     => 'theme-settings',
    ));
}

/*SVG support*/
function aviakvartal_svg_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'aviakvartal_svg_types', 99);

/*JS variables*/
function aviakvartal_js_variables() {
?>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
<?php
}
add_action('wp_footer','aviakvartal_js_variables');

/*Disable Gutenberg editor*/
add_filter('use_block_editor_for_post', '__return_false', 10);
add_filter('use_block_editor_for_post_type', '__return_false', 10);