(function($) {

    var position = $(window).scrollTop(); 
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if(scroll > position) {
            $('header').addClass('hide');
        } else {
            $('header').removeClass('hide');
        }
        position = scroll;
    });

    $(window).on('scroll', function(){
        let topScroll = $(document).scrollTop();
    
        $('.scroll').each(function(){

            var sectionScrol = $(this).offset().top,
                sectionHeight = $(this).height(),
                sectionActiveheight = sectionScrol + sectionHeight;
            if( topScroll + 10 >= sectionScrol && topScroll < sectionActiveheight ){
                var activeSection = '#' + $(this).attr('id');

                $('.main__nav li, .mob__nav li').each(function(){
                    var linkHref = $(this).find('a').attr('href');
                    linkHref = '#' + linkHref.substring(linkHref.indexOf("#") + 1);
                    
                    if( linkHref == activeSection ){
                        $(this).find('a').addClass('active');
                    } else {
                        $(this).find('a').removeClass('active');
                    }
                });
            }
        })
    });

    $(window).on('load', function(){
        $('.preloader__wrapper').addClass('hide');
        /*Mobile menu*/
        $('.mobile__btn').on('click', function(){
            $(this).toggleClass('show__menu');
            $('.mobile__navigation').toggleClass('show__navigation');
        });
        /*Scroll menu*/
        $(document).on('click', '.main__nav a, .mob__nav a, .main__slider a[href^="#"]', function (event) {
            var link = $(this).attr('href');

            $('.mobile__btn').removeClass('show__menu');
            $('.mobile__navigation').removeClass('show__navigation');

            if( link.indexOf("#") != '-1' ){
                link = '#' + link.substring(link.indexOf("#") + 1);

                event.preventDefault();
                $(this).addClass('active');
                $('.main__nav a').not(this).removeClass('active');

                $('html, body').animate({
                    scrollTop: $(link).offset().top 
                }, 500);
            }
        });

        /*Sliders*/
        if($('.main__slider').length){
            let $mainSlider = {
                infinite:           true,
                autoplay:           true,
                autoplaySpeed:      7000,
                speed:              1500,
                dots:               false,
                fade:               true,
                slidesToShow:       1,
                slidesToScroll:     1,
                prevArrow:          $('.main__slider__nav .prev__slide'),
                nextArrow:          $('.main__slider__nav .next__slide'),
                asNavFor:           '.thumbnails__slider',
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            dots: true,
                            arrows: false
                        }
                    }
                ]
            };

            let $thumbnailsSlider = {
                infinite:           true,
                autoplay:           true,
                autoplaySpeed:      7000,
                speed:              1500,
                centerMode:         true,
                centerPadding:      '0px',
                slidesToShow:       3,
                slidesToScroll:     1,
                dots:               false,
                arrows:             false,
                focusOnSelect:      true,
                asNavFor:           '.main__slider',
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2
                        }
                    }
                ]
            }
            $('.main__slider').slick($mainSlider);
            $('.thumbnails__slider').slick($thumbnailsSlider);
        }

        if($('.documents__slider').length){
            let $slider = {
                infinite:           true,
                autoplay:           true,
                autoplaySpeed:      8000,
                speed:              1500,
                dots:               true,
                arrows:             true,
                slidesToShow:       3,
                slidesToScroll:     3,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false
                        }
                    }
                ]
            };
            $('.documents__slider').slick($slider);
        }

        /*Popup*/
        $('.popup__btn').on('click', function(){
            $('.popup__wrapper').addClass('show__popup');
            $('.popup__block').addClass('show');
        });
        $('.close__popup').on('click', function(){
            $('.popup__wrapper').removeClass('show__popup');
            $('.popup__block').removeClass('show');
        });
        /*Tabs*/
        $('.tab').on('click', function(){
            var thisTab = $(this).closest('.tabs__nav').find('.tab').index(this);
            
            $(this).addClass('active');
            $(this).closest('.tabs__nav').find('.tab').not(this).removeClass('active');
            $(this).closest('.tabs').find('.tab__block').removeClass('active');
            $(this).closest('.tabs').find('.tab__block').eq(thisTab).addClass('active');
        });

        $(function() {
            var marker = [], infowindow = [], map;

            function addMarker(location,name,contentstr){
                marker[name] = new google.maps.Marker({
                    position: location,
                    map: map,
                    icon: {
                        path: 'M11 2c-3.9 0-7 3.1-7 7 0 5.3 7 13 7 13 0 0 7-7.7 7-13 0-3.9-3.1-7-7-7Zm0 9.5c-1.4 0-2.5-1.1-2.5-2.5 0-1.4 1.1-2.5 2.5-2.5 1.4 0 2.5 1.1 2.5 2.5 0 1.4-1.1 2.5-2.5 2.5Z',
                        scale: 2.0909090909090909090909090909,
                        anchor: new google.maps.Point(11, 22),
                        fillOpacity: 1,
                        fillColor: '#f44336',
                        strokeOpacity: 0
                    }
                });
                marker[name].setMap(map);

                infowindow[name] = new google.maps.InfoWindow({
                    content:contentstr
                });
                
                google.maps.event.addListener(marker[name], 'click', function() {
                    infowindow[name].open(map,marker[name]);
                });
            }

            function showGroupMarker(icon,location,name,contentstr){
                marker[name] = new google.maps.Marker({
                    position: location,
                    map: map,
                    icon: icon,
                    animation: google.maps.Animation.DROP
                });
                marker[name].setMap(map);

                infowindow[name] = new google.maps.InfoWindow({
                    content:contentstr
                });
                
                google.maps.event.addListener(marker[name], 'click', function() {
                    infowindow[name].open(map,marker[name]);
                });
            }

            function initialize() {

                var lat = $('#map-canvas').attr("data-lat");
                var lng = $('#map-canvas').attr("data-lng");

                var myLatlng = new google.maps.LatLng(lat,lng);

                var setZoom = parseInt($('#map-canvas').attr("data-zoom"));

                var mapOptions = {
                    zoom: setZoom,
                    disableDefaultUI: false,
                    scrollwheel: false,
                    zoomControl: true,
                    streetViewControl: true,
                    center: myLatlng,
                    styles: [{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#e0efef"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"hue":"#1900ff"},{"color":"#c0e8e8"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":700}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#7dcdcd"}]}]
                };
                map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

                $('.location__list a').each(function(){
                    let mark_lat = $(this).attr('data-lat'),
                        mark_lng = $(this).attr('data-lng'),
                        this_index = $('.location__list a').index(this),
                        mark_name = 'template_marker_'+this_index,
                        mark_locat = new google.maps.LatLng(mark_lat, mark_lng),
                        mark_str = $(this).attr('data-string');

                    addMarker(mark_locat,mark_name,mark_str);   
                });

                $('.map__sorting li').on('click', function(){
                    map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
                    $('.location__list a').each(function(){
                        let mark_lat = $(this).attr('data-lat'),
                            mark_lng = $(this).attr('data-lng'),
                            this_index = $('.location__list a').index(this),
                            mark_name = 'template_marker_'+this_index,
                            mark_locat = new google.maps.LatLng(mark_lat, mark_lng),
                            mark_str = $(this).attr('data-string');

                        addMarker(mark_locat,mark_name,mark_str);   
                    });
                        
                    $(this).addClass('active');
                    $('.map__sorting li').not(this).removeClass('active');

                    let marker  = $(this).find('.marker'),
                        icon    = $(this).find('.markers').data('marker');
                    marker.each(function(){
                        mark_lat = $(this).attr('data-lat'),
                        mark_lng = $(this).attr('data-lng'),
                        this_index = $(this).closest('.markers').find('.marker').index(this),
                        mark_name = 'template_marker_'+this_index,
                        mark_locat = new google.maps.LatLng(mark_lat, mark_lng),
                        mark_str = $(this).attr('data-string');
                        showGroupMarker(icon,mark_locat,mark_name,mark_str);
                    });
                });

            }
            
            if ($('.map__wrapper').length){    
                setTimeout(function(){
                    initialize();
                }, 500);
            }
        });

        AOS.init();
    });
})(jQuery);