<?php
/**
 *
 * @package WordPress
 * @subpackage Aviakvartal
 * @since 1.0
 * @version 1.0
 */
$popup = get_field('popup', 'option');
?>  
    </main>
    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <?php if( get_field('logo', 'option') ) { 
                        $logo = get_field('logo', 'option'); ?>
                        <a class="logo float-left" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <img src="<?php echo $logo['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                        </a>
                    <?php } 
                    wp_nav_menu( array(
                        'theme_location'        => 'main',
                        'container'             => 'nav',
                        'container_class'       => 'main__nav'
                    ) );
                    if( get_field('facebook', 'option') || get_field('instagram', 'option') ) { ?>
                        <div class="social float-right">
                            <?php if( get_field('facebook', 'option') ) { ?><a href="<?php the_field('facebook', 'option'); ?>" target="_blank" class="fb"></a><?php } ?>
                            <?php if( get_field('instagram', 'option') ) { ?><a href="<?php the_field('instagram', 'option'); ?>" target="_blank" class="in"></a><?php } ?>
                        </div>
                    <?php } ?>
                </div>  
            </div>
        </div>
    </footer>
    <div class="popup__wrapper">
        <?php if( $popup ) { ?>
            <div class="popup__block">
                <span class="close__popup"></span>
                <div class="title text-center">
                    <?php if( $popup['title'] ) { ?><h2><?php echo $popup['title']; ?></h2><?php } ?>
                    <?php if( $popup['subtitle'] ) { ?><p><?php echo $popup['subtitle']; ?></p><?php } ?>
                </div>
                <?php if( $popup['form_shortcode'] ) echo do_shortcode( $popup['form_shortcode'] );  ?>
            </div>
        <?php } ?>
    </div>
    <?php wp_footer(); ?>
</body>
</html>