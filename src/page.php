<?php
/**
 *
 * @package WordPress
 * @subpackage Aviakvartal
 * @since 1.0
 * @version 1.0
 */
get_header(); 

if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'main_slider' ): 
            get_template_part( 'inc/acf-content/main_slider' );
        elseif( get_row_layout() == 'about_complex_section' ): 
            get_template_part( 'inc/acf-content/about_complex_section' );
        elseif( get_row_layout() == 'benefits_section' ): 
            get_template_part( 'inc/acf-content/benefits_section' );
        elseif( get_row_layout() == 'planning_section' ): 
            get_template_part( 'inc/acf-content/planning_section' );
        elseif( get_row_layout() == 'infrastructure_section' ): 
            get_template_part( 'inc/acf-content/infrastructure_section' );
        elseif( get_row_layout() == 'building_section' ): 
            get_template_part( 'inc/acf-content/building_section' );
        elseif( get_row_layout() == 'documentations_section' ): 
            get_template_part( 'inc/acf-content/documentations_section' );
        elseif( get_row_layout() == 'form_section' ): 
            get_template_part( 'inc/acf-content/form_section' );
        elseif( get_row_layout() == 'contact_section' ): 
            get_template_part( 'inc/acf-content/contact_section' );
        endif;
    endwhile;
else :
    echo '
        <section class="page__section">
            <div class="page__content">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="no__content">
                                <h1>'.__('Nothing to show', 'aviakvartal').'</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    ';
endif;

get_footer();