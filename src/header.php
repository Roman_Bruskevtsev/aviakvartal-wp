<?php
/**
 *
 * @package WordPress
 * @subpackage Aviakvartal
 * @since 1.0
 * @version 1.0
 */

$wpml_lang = icl_get_languages();
$cur_lang_li = '';
$all_lang_li = '';
$lang_mobile = '';
foreach ($wpml_lang as $lang ) {
    if($lang['active']){
        $cur_lang_li = $all_lang_li .= '
                        <li class="active">
                            <span>'.$lang['code'].'</span>
                        </li>';
    } else {
        $all_lang_li .= '<li>
                            <a href="'.$lang['url'].'" class="wpml_'.$lang['code'].'">
                                <span>'.$lang['code'].'</span>
                            </a>
                        </li>';
    }
} 

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>
<body <?php body_class('load');?>>
    <div class="preloader__wrapper">
        <div class="transition-loader">
            <div class="transition-loader-inner">
                <label></label>
                <label></label>
                <label></label>
                <label></label>
                <label></label>
                <label></label>
            </div>
        </div>
    </div>
    <header id="header" class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <?php if( get_field('logo', 'option') ) { 
                        $logo = get_field('logo', 'option'); ?>
                        <a class="logo float-left" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <img src="<?php echo $logo['url']; ?>" alt="<?php echo get_bloginfo('name'); ?>">
                        </a>
                    <?php } 
                    wp_nav_menu( array(
                        'theme_location'        => 'main',
                        'container'             => 'nav',
                        'container_class'       => 'main__nav'
                    ) ); ?>
                    <div class="additional__nav float-right">
                        <?php if( get_field('phone_number', 'option') ) { ?>
                            <a class="phone" href="tel:<?php the_field('phone_number', 'option'); ?>">
                                <?php the_field('phone_number', 'option'); ?>
                            </a>
                        <?php } ?>
                        <ul class="lang__menu">
                            <?php echo $all_lang_li; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php if( get_field('phone_number', 'option') ) { ?>
            <a class="phone__icon" href="tel:<?php the_field('phone_number', 'option'); ?>"></a>
        <?php } ?>
        <div class="mobile__btn">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </header>
    <div class="mobile__navigation">
        <ul class="lang__menu">
            <?php echo $all_lang_li; ?>
        </ul>
        <?php wp_nav_menu( array(
            'theme_location'        => 'main',
            'container'             => 'nav',
            'container_class'       => 'mob__nav'
        ) ); 
        
        if( get_field('phone_number', 'option') ) { ?>
            <a class="phone float-right" href="tel:<?php the_field('phone_number', 'option'); ?>">
                <?php the_field('phone_number', 'option'); ?>
            </a>
        <?php } ?>
    </div>
    
    <main>